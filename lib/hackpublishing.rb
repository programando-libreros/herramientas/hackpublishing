require "hackpublishing/credits"
require "hackpublishing/go"
require "hackpublishing/help"
require "hackpublishing/init"
require "hackpublishing/log"
require "hackpublishing/toc"
require "hackpublishing/verify"
require "hackpublishing/version"

module Hackpublishing
  class Error < StandardError; end
end
