require 'hackpublishing/common'
require 'hackpublishing/nav'

module Hackpublishing
  class Go

    @@lessons = get_lessons

    # Identifies current lesson
    def initialize
      $log.save(self, __method__)
      inside?(3)

      @nav   = Hackpublishing::Nav.new
      id     = ARGV[1] ? ARGV[1] : @nav.get_current
      lesson = get_lesson(id) # TODO: solved ? next : this

      # Displays error if lesson doesn't exist
      if !@@lessons.any? {|l| l[:id] == id}
        $log.save(self, 'I18n.t(:errors)[:lesson]')
        puts I18n.t(:errors)[:lesson].colorize(:red)
        abort
      end

      self.display(id)
      @nav.set_current(id)

      # Completes lesson if no solution available
      if !lesson[:solution] then @nav.set_completed(id) end
    end

    # Prints lesson
    def display id
      $log.save(self, __method__)
      prt(get_lesson(id)[:path])
    end
  end
end
