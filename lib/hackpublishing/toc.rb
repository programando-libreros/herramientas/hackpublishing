require 'hackpublishing/common'
require 'colorize'

module Hackpublishing
  class Toc

    # Generates toc
    def initialize
      $log.save(self, __method__)
      inside?(3)

      @conf     = get_config
      @root     = get_root_txt
      lessons   = get_lessons
      completed = @conf['lessons']['completed'].sort
      slogan_h  = I18n.t(:slogans)[:header]
      slogan_f  = I18n.t(:slogans)[:footer].colorize(:blue)
      titles    = File.readlines(@root + 'lessons.md')

      # Prints header
      puts '', '╔ ' + slogan_h.colorize(:green) + ' ' + ('═' * (77 - slogan_h.length))

      titles.each_with_index do |t, i|
        # Prints title
        puts '║', '║ ' + t

        lessons.each do |l|
          if completed.include?(l[:id])
            crossed = 'x'
          else
            crossed = ' '
          end

          # Prints lessons
          if l[:id].to_i == i
            puts '║   ' + ('[' + crossed + '] ').colorize(:blue) + l[:name]
          end
        end
      end

      # Prints footer
      puts '║', '║ ' + slogan_f
      puts '║', '╚' + ('═' * 79)
    end
  end
end
