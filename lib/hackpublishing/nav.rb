require 'hackpublishing/common'
require 'hackpublishing/config'

module Hackpublishing
  class Nav
  
    @@lessons = get_lessons

    # Gets general data for navigation
    def initialize
      @conf = get_config
    end

    # Saves lesson as complete
    def set_completed id
      @conf['lessons']['completed'].push(id)
      @conf['lessons']['completed'].uniq!
      @conf['lessons']['completed'].sort!
      self.set_tries(0)
      if self.get_next(id, false) then self.set_current(self.get_next(id)) end
      Hackpublishing::Config.new.save(@conf)
    end

    # Saves current lesson id
    def set_current id
      if @conf['lessons']['current'] != id then self.set_tries(0) end
      @conf['lessons']['current'] = id
      Hackpublishing::Config.new.save(@conf)
    end

    # Refreshs tries
    def set_tries total
      @conf['lessons']['tries'] = total
      Hackpublishing::Config.new.save(@conf)
    end

    # Returns completed lessons
    def get_completed
      return @conf['lessons']['completed'].sort
    end

    # Returns current lesson id
    def get_current
      return @conf['lessons']['current']
    end

    # Returns next lesson id
    def get_next id, verbose = true
      type      = 'lesson'
      print     = verbose
      lessons   = get_lessons_ids
      position  = 'continue'
      available = lessons.reject{|l| self.get_completed.include?(l)}

      if available.length == 0
        type     = 'all'
        print    = true
        position = 'finished'
      elsif id.to_i != available.first.to_i
        type     = 'section'
      end

      if print then prt_success(type, position, id) end

      return available.length > 0 ? available.first : nil
    end

    # Returns current lesson tries
    def get_tries
      return @conf['lessons']['tries']
    end
  end
end
