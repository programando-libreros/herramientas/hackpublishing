require 'hackpublishing/common'
require 'hackpublishing/config'
require 'colorize'

module Hackpublishing
  class Init

    # Creates new study room
    def initialize lang = ''
      verbose = lang.length >= 2 ? false : true

      if verbose
        puts I18n.t(:welcome)
        lang = get_data(I18n.t(:questions)[0], get_langs)
        puts '', I18n.t(:init).colorize(:blue), ''
      end

      if get_langs.include?(lang) == false
        puts I18n.t(:errors)[:lang].colorize(:red)
        abort
      end

      conf = Hackpublishing::Config.new
      dir  = 'study-room'

      if Dir.exist?(dir)
        puts I18n.t(:errors)[:dir].colorize(:red)
      else
        Dir.mkdir(dir)
        $log.create(dir)
        $log.save(self, RUBY_PLATFORM + ' ruby' + RUBY_VERSION, dir + '/' + $logf)
        conf.create(dir, lang)
        prt('splash-screen')
        if verbose
          puts '', I18n.t(:ready)[0].colorize(:green)
          puts '  ' + I18n.t(:ready)[1].gsub('$dir', dir)
        end
      end
    end

    # Gets necessary data for config file
    def get_data question, opts = false
      print "\n", question.colorize(:green)

      if opts
        txt = ' ['
        opts.each do |opt| txt += opt + '/' end
        print (txt[0..-2] + ']').colorize(:green)
      end

      print "\n> "
      answer = STDIN.gets.chomp

      if opts && opts.include?(answer) == false
        puts '', I18n.t(:errors)[:opt].colorize(:red)
        get_data(question, opts) 
      else
        return answer
      end
    end
  end
end
