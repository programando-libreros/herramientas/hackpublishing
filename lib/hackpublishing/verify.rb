require 'hackpublishing/extensions'
require 'hackpublishing/common'
require 'hackpublishing/nav'

module Hackpublishing
  class Verify

    # Verifies current lesson
    def initialize
      $log.save(self, __method__)
      inside?(3)
      
      @nav      = Hackpublishing::Nav.new
      lesson    = get_lesson(@nav.get_current)
      @solution = get_solution(lesson)

      puts I18n.t(:verified)[:scan]
               .gsub('$id', @nav.get_current).colorize(:blue)

      # Verifies if solution available
      if !lesson[:solution]
        $log.save(self, 'I18n.t(:skip)[0]')
        puts '', I18n.t(:skip)[0].colorize(:green)
        puts '  ' + I18n.t(:skip)[1]
        abort
      end

      sol_p = @solution['path']    ? self.verify_path    : true
      sol_f = @solution['files']   ? self.verify_files   : true
      sol_m = @solution['matches'] ? self.verify_matches : true

      if sol_p && sol_f && sol_m
        @nav.set_completed(@nav.get_current)
      else
        $log.save(self, 'I18n.t(:errors)[:verify]' + "\n" +
          {'path' => sol_p, 'files' => sol_f, 'matches' => sol_m}.to_s)
        puts '', I18n.t(:errors)[:verify].colorize(:red)
        puts sol_p.to_box + I18n.t(:verified)[:v_path]
        puts sol_f.to_box + I18n.t(:verified)[:v_files]
        puts sol_m.to_box + I18n.t(:verified)[:v_matches]
        puts '', I18n.t(:repeat)[0].colorize(:green)
        puts '  ' + I18n.t(:repeat)[1]
        @nav.set_tries(@nav.get_tries + 1)
      end
    end

    # Verifies current path
    def verify_path path = @solution['path']
      path = File.dirname(path + '/..')
      root = File.expand_path(File.dirname(inside?(1)))
      pwd  = Dir.pwd.gsub(root, '.')
      return pwd == path ? true : false
    end

    # Verifies current files
    def verify_files files = @solution['files']
      all   = Dir.children(Dir.pwd)
      files = files.reject{|f| all.include?(f)}
      return files.length == 0 ? true : false
    end

    # Verifies current files content
    def verify_matches matches = @solution['matches']
      files = Dir.children(Dir.pwd)

      matches.each do |e|
        file = e.keys.first
        rgxs = e[file]
        cnt  = files.include?(file) ? File.read(file) : false

        # Verifies if the required file exists
        if !cnt then return false end

        # Verifies if the required content exists
        rgxs.each do |r|
          r = r.gsub(/^\//, '').gsub(/\/$/, '')
          if !cnt.match(/#{r}/) then return false end
        end
      end

      return true
    end
  end
end
