require 'hackpublishing/common'
require 'yaml'

module Hackpublishing
  class Config

    # Gets config sample
    def initialize
      @config = File.read($root + '../config/config.yml')
    end

    # Creates config file
    def create path = '.', lang = 'en'
      file = File.open(path + '/' + $conf, 'w:utf-8')
      file.puts @config
        .gsub('$lang', lang)
        .gsub(/^#.+?$\n/, '')
      file.close
    end

    # Saves config
    def save content
      $log.save(self, __method__.to_s + "\n  " + content.to_s)
      file = File.open(get_config(1), 'w:utf-8')
      file.puts content.to_yaml
      file.close
    end
  end
end
