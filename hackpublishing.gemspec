require_relative 'lib/hackpublishing/version'

Gem::Specification.new do |spec|
  spec.name          = "hackpublishing"
  spec.version       = Hackpublishing::VERSION
  spec.authors       = ["Programando LIBREros", "Perro Tuerto"]
  spec.email         = ["hi@programando.li"]

  spec.summary       = %q{Learn publishing in the hack way.}
  spec.description   = %q{This gem is an introductory manual to free publishing technologies.}
  spec.homepage      = "https://hack.programando.li/breros"
  spec.license       = "GPL-3.0-or-later"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.5.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://gitlab.com/programando-libreros/herramientas/hackpublishing"
  spec.metadata["changelog_uri"] = "https://gitlab.com/programando-libreros/herramientas/hackpublishing/-/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "rspec", "~> 3.9"
  spec.add_dependency "colorize", "~> 0.8.1"
  spec.add_dependency "i18n", "~> 1.8.3"
  spec.add_dependency "yaml", "~> 0.1.0"
end
