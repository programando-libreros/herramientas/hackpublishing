> **UNDER DEVELOPMENT**: keep in touch for new lessons and more!

![Splash screen](src/misc/splash-screen.jpg)

# Hackpublishing

[![Gem Version](https://badge.fury.io/rb/hackpublishing.svg)](https://badge.fury.io/rb/hackpublishing)

Learn publishing in the hack way. This gem is an introductory
manual to free publishing technologies.

## What is Hackpublishing?

This Ruby gem helps you to learn how to publish in the hack way >:)
To accomplish that goal, Hackpublishing requires a study
room where you will learn and practice.

Each study room is divided by sections. A section is an array
of lessons designed to teach you some specifics tasks. You can
do whatever lessons you want and in any order. But to accomplish
a lesson you have to verify it.

You can always see the table of contents, so you can decide which
is gonna be your next challenge!

By design this gem doesn't grade lessons (who likes grades, anyways?),
it only verifies if they have been accomplished or not. It does
saves the number of tries, but just to detect which lesson has
been particularly challenging, so we can evaluate if we have
to fix it.

## Installation

Inside the terminal run:

```
gem install hackpublishing
```

## TODO

Check the [To Do](https://gitlab.com/groups/programando-libreros/herramientas/-/boards?scope=all&utf8=%E2%9C%93&search=Lesson) list!

## Usage

```
hackpublishing [ARGUMENTS]

Go to a lesson:
  hackpublishing go
    => Goes and displays the next available lesson.
  hackpublishing go LESSON_NUMBER
    => Goes and displays the required lesson.

See table of contents:
  hackpublishing toc
    => Displays the table of contents.

Verify lessons:
  hackpublishing verify
    => Verifies the current lesson.

Set a study room:
  hackpublishing init
    => Initializes a new study room in interactive mode.
  hackpublishing init LANG_AVAILABLE
    => Initializes a new study room in quiet mode.

Manage the log:
  hackpublishing log
    => Displays study room' log.
  hackpublishing log clean
    => Cleans study room' log.

See credits:
  hackpublishing credits
    => Displays gem's credits and licenses.

See help:
  hackpublishing help
    => Displays this help.
```

## Development

After checking out the repo, run `bin/setup` to install dependencies.
Then, run `rake spec` to run the tests. You can also run `bin/console`
for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec
rake install`. To release a new version, update the version number
in `version.rb`, and then run `bundle exec rake release`, which
will create a git tag for the version, push git commits and tags,
and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on [GitLab](https://gitlab.com/programando-libreros/herramientas/hackpublishing).

### Write or translate lessons

If you want to contribute by writing more lessons or translate
some, you can do it! You just need to know [Markdown](https://en.wikipedia.org/wiki/Markdown)
and [YAML](https://en.wikipedia.org/wiki/YAML).

#### Step by step for writing or translating lessons

##### 1. Change to `develop` branch

The `master` branch is the stable version of this gem. All contribution are
handled in its `develop` branch. To switch branch, do:

```
git checkout develop
```

##### 2. Go to source

In order to write lessons you will have to work in [`src`](https://gitlab.com/programando-libreros/herramientas/hackpublishing/-/tree/master/src).

There you have the following structure:

```
src => Source directory
├── misc => Directory for assets
├── txt => Directory for MD files
│   ├── en
│   ├── es
│   └── ...
└── yml => Directory for YAML files
    ├── en
    ├── es
    └── ...
```

As you can see, each lesson is divided by language. Inside each
language directory you will find the MD or YAML files. If you
need to add other type of files, the `misc` directory is the
place to put them.

##### 3. Create files

**If you want to extend Hackpublishing lessons**, you will have
to create two files *for each language*:

1. MD file for the content of the lesson.
2. YAML file for the solution sheet of the lesson.

Each file requires specific file names:

* MD file name is `lesson_` + section number + `-` + lesson number + `.md`
* YAML file name is `solution_` + section number + `-` + lesson number + `.yml`

For example, if you want a write a lesson 0 for section 1, the
file names should be:

* `lesson_1-0.md` for the MD file.
* `solution_1-0.yml` for the YAML file.

Each file has to be in its correct directory *for each language*:

* MD files in `txt` + language directory. 
* YAML files in `yml` + language directory. 

**If you want to extend Hackpublishing language support**, just
make sure you create new directories in `txt` and `yml` for the
new language in [ISO 639-1 code](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).

> If you can't write or translate a file in an specific language, just leave it
> without translation but following the convention mentioned above.

##### 4. Edit files

Now you can start editing the MD and YAML files, just keep in
mind the following.

**MD files** only support this syntax:

* ATX-style headers
* Blockquotes
* Fenced code blocks
* Comment blocks
* Italic
* Bold
* Marks
* Inline code
* Links

The supported syntax is going to be displayed with particular
styles. The unsupported syntax is going to be displayed as it
is.

**YAML files** require this specific structure:

```
---
# Empty sample
version: 1.0
path:
files:
matches:
```

YAML files allows three types of data:

* `path`: required position from study room root. Values: `String` or `Nil`.
* `files`: required files in working directory. Values: `Array` or `Nil`.
* `matches`: required matches inside file in working directory. Values: `Array of Objects` or `Nil`.

Respectively, this allows you to verify three aspects:

1. Where is the user located?
2. Which files exists in the current location?
3. Which file contents has to match for each file in the current location?

Let's see and example to better understand:

```
---
version: 1.0
path: './dir1/'
files:
- dir2
- file1.md
- file2.md
matches:
- file1.md:
  - "Hello, World!"
- file2.md:
  - /^#\s+/
  - \n{2,}>\s*
```

In this example the user requires the following so `hackpublishing
verify` sets to true:

* The user has to be in `dir1` inside the study room.
* The current directory need to have this children: `dir2`, `file1.md` and `file2.md`.
* The content of `file1.md` has to match with `Hello, World!`.
* The content of `file2.md` has to match with `/^#\s+/` and `\n{2,}>\s*`.

This enables the following clarifications:

* Leave any value empty for the key (`path`, `files` or `matches`) that you don't want to verify.
* For matches you can use strings or RegEx formulas (surrounded or not by `/`).

##### 5. Verify section headers

Now is time to verify the section headers. The headers of each
section are printed when the users runs `hackpublishing toc`.
Remember, *each lesson is always part of a section*.

To verify the section headers you have to open and edit the `lessons.md`
for *each language*.

The section header is written as a Markdown numbered list. For
example:

```
0. This is Hackpublishing!
1. Another section header
2. Another one
```

##### 6. Test!

The last step for writing or translating lessons is testing!

Locally build and install the gem to see if everythings works
as expected:

```
git add . && gem build hackpublishing.gemspec && gem install hackpublishing
```

If everything works, send your pull requests!

## License

The gem is available as free software under the terms of the
[GNU General Public License version 3](https://opensource.org/licenses/GPL-3.0).

The text of the lessons are under [Open and Free Publishing License
(LEAL)](https://gitlab.com/programando-libreros/juridico/licencia-editorial-abierta-y-libre)

## Contact

Do you need more support? Email us at [hi@programando.li](mailto:hi@programando.li) :)
