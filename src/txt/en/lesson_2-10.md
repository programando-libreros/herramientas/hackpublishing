# 2.10. Mi primer desafío

La navegación ha sido sencilla. ¿Quieres seguir avanzando? Intenta ejecutar de
nuevo `$ … go`… ¡Sorpresa! no importa cuánto hagas `$ … go`, esto aparece otra
vez. Además, si me saltas y haces `$ … toc`, esta lección no está marcada.

¿A qué se debe? Una lección solo se marca cuando ocurre uno de estos dos casos:

1. La lección no requiere solución, por lo que automáticamente queda marcada.
   (Como ocurrió en el pasado).
2. La lección requiere solución, así que tienes que solucionarla para marcarla.
   (Como ocurre ahora).

Entonces, para solucionar tu primer desafío, haz lo siguiente:

1. Crea un nuevo directorio que se llame `seccion02`.
2. Ejecuta `$ … verify`.
3. Avanza con `$ … go`.

> **ProTip.** Para (1) ejecuta `$ mkdir seccion02`.
