```
 __________                                _nnnn_      ___
/ ======== \                              dGGGGMMb     \_/
____________\                            @p~qp~~qMb     |._
____________ |                    _,)    M|@||@) M|     |'."-._.-""--.-"-.__.-'/
|ssh dog@  | |            _..._.-;-'     @,----.JM|     |  \       .-.        (
|community | |         .-'     `(       JS^\__/  qKL    |   |     (@.@)        )
|__________| |        /      ;   \     dZP        qKRb  |   |   '=.|m|.='     /
_____________/       ;.' ;`  ,;  ;    dZP          qKKb |  /    .='`"``=.    /
"""""""""""" \      .'' ``. (  \ ;   fZP            SMMb|.' GNU/Linux Rules!(
::::::::::::: \    / f_ _L \ ;  )\   HZM            MMMM|.-"-.__.-""-.__.-"-.)
_______________)   \/|` '|\/;; <;/   FqM            MMMM|
                  ((; \_/  (()     __| ".        |\dS"qML
      |\      _,,,---,,_           |    `.       | `' \Zq
ZZZzz /,`.-'`'    -.  ;-;;,_      _)      \.___.,|     .'
     |,4-  ) )-,_. ,\ (  `'-'     \____   )MMMMMM|   .'
    '---''(_/--'  `-'\_)               `-'       `--'
```

# 0.2. A dog on the internet met hackers, cats and pirates…

( Perro Tuerto es el nombre para el colectivo compuesto por una sola persona.
( Su origen viene del fallido colectivo Perro Triste, en el que hubo más de una
( persona involucrada. Perro Triste surgió con la idea de que otra cultura
( literaria es posible. Se encontraba triste por la manera en como la cultura
( hegemónica trata a quienes nos acercamos por algún ideal distinto al dinero.
( Este colectivo tuvo un rotundo fracaso por no saber cómo trabajar en equipo.
( Al final no es algo que nos hayan enseñado en la escuela, sino que lo hemos
( ido aprendiendo a lo largo de los diversos proyectos de los que hemos sido
( parte.

( De ese fracaso nació el «unicolectivo» Perro Tuerto. Ahora el perro está
( tuerto porque perdió un ojo en su batalla por la autonomía y soberanía en la
( producción cultural. En su lucha también conoció a una nueva aliada, Mel
( Bayardo, que en unidad decidimos llamarnos Programando LIBREros. El nombre es
( un tanto obvio, pero déjame aclararlo. «Programando libreros» porque para
( nosotros _la edición se hizo código_. Con esto no nos referimos a que la
( edición, tal como la conocemos, ha muerto. Pero sí deseamos hacer énfasis en
( que en la actualidad la industria editorial está en una dura fase de
( transformación que no se había visto desde al menos el advenimiento de la
( imprenta. Por eso, para nosotros _programar_ es una nueva modalidad para
( _editar_. Además, «programando libre» debido a que en nuestro quehacer solo
( usamos _software_ libre o de código abierto (FOSS, por sus siglas en inglés).
( El concepto de «_software_ libre» puede sonar muy ambiguo y tal vez siempre
( sea así, por el momento no indagaremos más en ello.

( Mel y este perro no somos los únicos aliados. En nuestro paso hemos conocido
( muchas personas hacker, como los compas de Colima Hacklab o del Rancho
( Electrónico, quienes siempre nos han dado su apoyo incondicional. También
( hemos conocido personas en el grupo de Telegram @miau2018, en cuya descripción
( dice: «Edición libre y bibliotecología DIY desde la tecnopolítica, la
( experiencia latinoamericana y los feminismos». En este grupo lleno de gatitos
( nos concentramos diversas personas que no comparten los ideales de la
( producción, reproducción, distribución y conservación (PRDC) cultural
( hegemónica, especialmente en la que se refiere a la edición de libros y
( bibliotecología. Por último y no menos importantes están todos esos piratas,
( como los del Partido Interdimensional Pirata de Argentina, que con sus
( esfuerzos y conocimientos nos han demostrado que otros paradigmas en la
( cultura son posibles, es solo de dedicarle tiempo y esfuerzo.

( Este manual es precisamente un punto de entrada o de encuentro a estas otras
( formas de hacer cultura: más técnica, más experimental, más soberana y, por
( supuesto, más diversa que lo que aprendemos en la escuela o en la industria.
