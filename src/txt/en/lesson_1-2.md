# 1.2. ¿Por qué «manual introductorio»?

Aunque queramos ofrecerte todo el conocimiento que hemos albergado, mantenido y
construido en nuestra comunidad, su tipo de producción nos impide presentártelo
a modo de compendio o recetario. Muchas cosas únicamente las aprenderás en la
práctica, con especial énfasis en el yerro.

En este manual solo te podemos introducir a procesos y técnicas que nos permiten
producir, reproducir, distribuir y conservar publicaciones. Tú serás quien
encuentre maneras de adaptar las enseñanzas a tus necesidades.

Es decir, el manual es el inicio de una aventura cuyo fin es desprenderte de
esta herramienta para relacionarte con quienes están entre senderos similares:
los hackeditores.
