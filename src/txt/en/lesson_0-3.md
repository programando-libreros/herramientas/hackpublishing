```
                  .-----.░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░░
       .--.       |~~~~~|░░░░░░░░█████░░░░░.--> $ echo "How to make a book?"
       |__|       |~~~~~|░░░░░░░░█ H █░░░.'░░░░ $ vim book.md
.--.---|--|_      |     |--.░░░░░█ A █<-'░░░░░░ $ pandoc book.md -o book.pdf
|  |===|  |'\     | PHI |--|░░░░░█ C █░░░░░░░░░ $ pecas book.md
|%%|===|  |.'\    | LO  |  |<--->█ K █░░░░░░░░░ $ ls
|%%| A |  |\.'\   | SO  |  |░░░░░█ S █░░░░░░░░░ book.epub  book.mobi
|  | R |  | \  \  | PHY |  |░░░░░█████░░░░░░░░░ book.html  book.pdf
|  | T |__|  \.'\ |     |__|░░░░░░░'.░░░░░░░░░░ $ git add .
|  |===|--|   \.'\|~~~~~|--|░░░░░░░░░|░░░░░░░░░ $ git commit -m "Lets share!"
^--^---'--^    `-'^-----'--'░░░░░░░░░V░░░░░░░░░ $ git push origin master
```

# 0.3. And together they devised another world to make books…

( Para Programando LIBREros fueron las artes y la filosofía lo que nos trajo,
( primero, al mundo de los libros y, después, al mundo de la técnica para hacer
( libros y, por último, al mundo del desarrollo tecnológico de herramientas
( editoriales.

( Pero los caminos para la «hackedición» son muy diversos. En nuestra comunidad
( conocemos personas que llegaron por la necesidad de utilizar otras
( herramientas, por el interés en otras áreas, como la computación, la
( antropología o la sociología, o hasta por el mero afán de compartir
( información considerada relevante.

( En este manual solo probarás algunas vías para poder hacer, deshacer o
( rehacer libros. El uso de la herramienta o el conocimiento de los procesos no
( es más relevante que la comprensión de que los formatos que utilizas y los
( recursos de cómputo con los que cuentas no limitan tus posibilidades de
( PRDC cultural, aunque quizá sí limite tu visibilidad en el lado A de internet.
