# 2.7. Mi recordatorio

Ahora ya sabes usar `$ … go` y `$ … toc` y, como ves, `$ hackpublishing` tiene
más comandos que puedes pasar como argumentos.

Otro comando muy importante es `$ … help`, el cual te imprime un recordatorio
de todos los comandos que puedes usar con `hackpublishing`. Inténtalo y después
avanza a la siguiente sección.
