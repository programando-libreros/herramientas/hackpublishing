# 2.9. Mis soluciones

Si bien Hackedición no emplea ningún mecanismo de calificación (eso es cosa del
pasado), sí tiene un sistema para motivarte a hacer los ejercicios.

Hasta ahora los ejercicios han sido pensados para familiarizarte con tu sala de
estudio. Pero ahora es momento de pasar a la última fase de este proceso:
aprender a solucionar lecciones.

Es decir, en ciertas lecciones tienes que hacer alguna de estas cosas:

- Ir a un lugar específico **entre tus directorios**.
- Crear archivos específicos **adentro de tu directorio de trabajo**.
- Colocar información específica **en ciertos archivos**.

El comando `$ … verify` analiza tus soluciones: te indica si son correctas o no.
Además, `$ … verify` te imprime una lista de cosas resueltas.

Solo cuando solucionas una lección puedes hacer `$ … go` para ir a la siguiente
lección. De lo contrario, `$ … go` te vuelve a imprimir la misma lección.
