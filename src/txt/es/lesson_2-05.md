# 2.5. ¡Un salto!

> **OJO.** Si llegaste aquí por ejecutar `$ hackpublishing go`, ejecuta
> `$ hackpublishing go 2.3` y vuelve a leer con detenimiento.

Mira nada más, si vienes de la sección `$ … 2.3` es porque ejecutaste
`$ hackpulishing go 2.5`. Si es así, entonces ya sabes ir a `$ … 2.4`.
Inténtalo si escribes `$ hackpublishing go 2.4`.
