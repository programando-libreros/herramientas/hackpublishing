```
   ____________________________________________________________________________
 /                                                                             \
|    _____________________________________________________________________     |
|   |                                                                     |    |
|   |  $ créditos                                                         |    |
|   |                              Hackedición                            |    |
|   |        Manual introductorio a tecnologías editoriales libres        |    |
|   |          www.hack.programando.li/breros | hi@programando.li         |    |
|   |                                                                     |    |
|   |  Producto ofrecido por                                              |    |
|   |    Programando LIBREros <www.programando.li/breros>                 |    |
|   |  Proyecto apoyado por el                                            |    |
|   |    Fondo Nacional para la Cultura y las Artes (FONCA)               |    |
|   |  Programa gratuito, abierto y libre desarrollado por                |    |
|   |    Perro Tuerto <www.perrotuerto.blog>                              |    |
|   |                                                                     |    |
|   |  Testers: Mel, Antílope                                             |    |
|   |                                                                     |    |
|   |  Escribe nuevas lecciones o edita y traduce secciones enteras en:   |    |
|   |  www.gitlab.com/programando-libreros/herramientas/hackpublishing    |    |
|   |                                                                     |    |
|   |      Hecho con Ruby | Licencias: GPLv3 (código) y LEAL (texto)      |    |
|   |_____________________________________________________________________|    |
|                                                                              |
 \____________________________________________________________________________/
               \_______________________________________________/
                _______________________________________________
             _-'    .-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.  --- `-_
          _-'.-.-. .---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.--.  .-.-.`-_
       _-'.-.-.-. .---.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-`__`. .-.-.-.`-_
    _-'.-.-.-.-. .-----.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-----. .-.-.-.-.`-_
 _-'.-.-.-.-.-. .---.-. .-----------------------------. .-.---. .---.-.-.-.`-_
:-----------------------------------------------------------------------------:
`---._.-----------------------------------------------------------------._.---'
```
