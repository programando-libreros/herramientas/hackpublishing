# 2.6. Mi mapa

El comando `$ … go` es muy útil para moverte o saltar entre lecciones. Pero no
hace nada más. ¿Qué pasa si quieres ver cuántas lecciones o secciones existen,
cuáles has terminado y cuáles te faltan completar?

Con `$ … go` no obtendrás la respuesta. Pero si sabes usar `$ … go`, ahora sabes
ejecutar `$ … toc`. Inténtalo y después avanza a la siguiente sección.
