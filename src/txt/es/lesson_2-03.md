# 2.3. Mi primer salto

¡Muy bien! Si sabes ejecutar `$ hackpublishing go`, entonces sabes más cosas de
las que crees.

Ya sabes que `$` indica el fin de tu _prompt_. Ahora sabes que `hackpublishing`
es el nombre del programa que ejecutas al escribir `$ hackpublishing go`.

Cuando ejecutas un programa por lo general tienes que indicar ==argumentos== para
hacer distintas cosas con él. Por ejemplo, con `$ hackpublishing go` le indicas
a `hackpublishing` que `go` (vaya) a la siguiente lección. Es decir, `go` es un
argumento de `hackpublishing`.

A un programa le puedes indicar varios argumentos. Con `$ hackpublishing go`
solo fue un argumento (`go`) para ir a la siguiente leccción. Pero ¿qué pasa si
si quieres saltar entre lecciones?

Si sabes ejecutar `$ hackpublishing go` y si sabes que puedes escribir más
argumentos, entonces seguro ya puedes saltar a la sección `$ … 2.5`. Inténtalo
si escribes `$ hackpublishing go 2.5`.
