```
  ,\//,.\//\/.         ,\/,   ,\/.//,                           |       
  //o\\/o//o\\ ,.,.,   //o\   /o\\o\\                       \       /   
    |   |  |  /###/#\    |     |  |                           .-'-.     
____|___|__|__|'_'|:|____|`=.='|__|____________          --  /     \  --
                                               `~^~^~^~^~~^~^~^~^~^~^~^~^~^~^~^~
```

# 0.1. Érase una vez por tierra de cocos…

( Por supuesto la historia es más compleja. Aunque a partir de aquí y a lo
( largo de estas lecciones se hablará de esta «comunidad» de la que Programando
( LIBREros forma parte, la realidad es que aún no terminamos de comprender a
( qué nos referimos cuando hablamos de «comunidad». Sin embargo, sí sabemos
( que en este lado B del internet hemos encontrado personas y colectivos que
( no solo comparten varios de nuestros ideales, sino que también nos han
( apoyado en nuestro aprendizaje y proyectos cuyas pretensiones son la
( posibilidad de otra manera de hacer cultura y, quizá, de otros mundos.
