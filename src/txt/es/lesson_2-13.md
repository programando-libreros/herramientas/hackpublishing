# 2.13. Mis habilidades

Ahora sabes lo que Hackedición es capaz de hacer. Es decir:

- Seguiste las instrucciones marcadas con números.
- Ejecutaste `$ … verify` para comprobar la solución.
- Fuiste a la siguiente lección con `$ … go`.
- Viste el mapa de tu sala de estudio con `$ … toc`.
- Accediste al recordatorio de los comandos de `hackpublishing` con `$ … help`.

En esta sección ejecutaste comandos que quizá no sabes cómo funcionan. No te
preocupes, es parte del aprendizaje. Lo importante son **las dos habilidades que
te permitirán avanzar con menos frustraciones**:

1. Lee todo lo que te arroje la terminal, **¡todo!**
2. Realiza las instrucciones que se te piden, **¡todas!**

Con estos elementos ya sabes cómo usar tu sala de estudio. ¡Bravo!
