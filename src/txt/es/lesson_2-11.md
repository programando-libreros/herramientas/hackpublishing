# 2.11. Mi segundo desafío

¡Uff! La lección pasada no fue un desafío sencillo. ¿Qué nos depara ahora?

Ya sabes que para solucionar una lección a veces tienes que crear ==ficheros==
(un término genérico para hablar de archivos o directorios).

Para esta lección tienes que meterte al directorio creado con anterioridad. Por
eso te voy a pedir:

1. Métete a `seccion02`.
2. Ejecuta `$ … verify`.

> **ProTip.** Para (1) ejecuta `$ cd seccion02`.
